from abc import ABC;

class Person(ABC):
    def get_full_name(self):
        pass
    def add_request(self):
        pass
    def check_request(self):
        pass
    def add_user(self):
        pass

class Employee(Person):
    def __init__(self,first_name,last_name,email,department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    def set_first_name(self, first_name):
        self._first_name = first_name
    
    def set_last_name(self, last_name):
        self._last_name = last_name
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self,department):
        self._department = department

    def check_request(self):
        print(f"Checking request")

    def add_user(self):
        print(f"Adding user")
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
         return f"{self._email} has logged out"
    
    def get_full_name(self):
        return self._first_name + " " + self._last_name
    
    def add_request(self):
        return "Request has been added"
    
    def check_request(self):
        return super().check_request()
    
    def add_user(self):
        return super().add_user()

class TeamLead(Person):
    def __init__(self,first_name,last_name,email,department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self.members = []

    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    def set_first_name(self, first_name):
        self._first_name = first_name
    
    def set_last_name(self, last_name):
        self._last_name = last_name
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self,department):
        self._department = department
    
    def add_member(self,employee):
        self.members.append(employee)
        return self.members
    
    def get_full_name(self):
        return self._first_name + " " + self._last_name

    def get_members(self):
        return self.members


class Admin(Person):
    def __init__(self,first_name,last_name,email,department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    def get_first_name(self):
        return self._first_name
    
    def get_last_name(self):
        return self._last_name
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    def set_first_name(self, first_name):
        self._first_name = first_name
    
    def set_last_name(self, last_name):
        self._last_name = last_name
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self,department):
        self._department = department

    def add_user(self):
        return ("User has been added")

    def get_full_name(self):
        return self._first_name + " " + self._last_name

class Request:
    def __init__(self,name,requester,date_requested):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested
        self.status = ""
    
    def update_request(self):
      return f"Request {self.name} has been {self.status}"
    
    def close_request(self):
       return f"Request {self.name} has been {self.status}"
    
    def cancel_request(self):
        return f"Request {self.name} has been {self.status}"
    
    def set_status(self,status):
        self.status = status

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())
    